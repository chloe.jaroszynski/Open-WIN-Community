---
layout: default
title: Edit your protocol
parent: Open MR Protocols
has_children: false
nav_order: 4
---


# Editing your protocol entry
{: .fs-9 }


---

<!-- ![open-protocols](../../../img/img-open-mrprot-flow.png) -->


If you are using the protocols database to track changes made to your protocol while piloting, you can amend the entry and create a new version that can be reviewed at any point.

![gif - update](../../../img/img-protocols/gif_update.gif)

1. Go to `my protocols` and find the entry you would like to update.
2. Select if you are adding a new protocol pdf file (`Update with new protocol file`) or making changes to other parts of the entry (`Update keeping existing protocol file`)
3. Identify if you are making a minor amendment to the descriptive text by incrementing the "edit" number only or a more substantial update by incrementing the "version" number.
4. Make the required changes.
5. We recommend adding a "Change Log" section to the `Description` field to record what changes have been made and why.
6. Save your edits using the `save` button at the bottom of the page.
7. You can review your edits by clicking on `full history` on any protocol entry.

### Transferring ownership
If you are unable to log in with an SSO (your Oxford account has expired), you will need to transfer the ownership of your protocol to someone else who can do it (potentially the Principal Investigator of the project). To arrange this, please contact admin@win.ox.ac.uk.
